# Firmwares for dongle

AI Lab dongle firmwares

Contains firmwares and models for dongle

## Firmwares

### Initial

Contains firmwares provided by kneron.

## Models

### Tiny_yolo_v3

Contains tiny yolo v3 model provided by kneron.
